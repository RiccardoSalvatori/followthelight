#include "Arduino.h"
#include "utils.h"

#define DEBOUNCE_DELAY 250
#define MAX_INTENSITY 255
#define FADING_SPEED 20

int fadeAmount = 5;
int currentIntensity = 0;

void fading(int led) {
  analogWrite(led, currentIntensity);
  currentIntensity = currentIntensity + fadeAmount;
  if (currentIntensity == 0 || currentIntensity == MAX_INTENSITY) {
    fadeAmount = -fadeAmount ;
  }
  delay(FADING_SPEED);
}


void blinkLed(int led, int delayTime) {
  digitalWrite(led, HIGH);
  delay(delayTime);
  digitalWrite(led, LOW);
  delay(delayTime);
}

boolean debouncedButton(unsigned long buttonLastDebounceTime) {
  return ((millis() - buttonLastDebounceTime) > DEBOUNCE_DELAY);
}
