/**

    Filippo Pronti, 0000759529

    Stefano Salvatori, 0000758583

    Riccardo Salvatori 0000759106

*/
#include "utils.h"

#define MAX_LIST_SIZE 500
#define POT_PIN A5
#define POT_RANGE 1023
#define MIN_SPEED 300
#define MAX_SPEED 1000
#define PLAYER_INPUT_TIME 5000
#define DT_GAME_OVER 1000
//Led pin
#define LED_FLASH 3
#define NUM_LEDS 3
#define LED1 9
#define LED2 10
#define LED3 11
//Button pin
#define T1 5
#define T2 6
#define T3 7

enum game_state {
  INIT,
  STARTED,
  SHOWING_LIGHTS,
  WAIT_USER,
  GAME_OVER
};

int leds[NUM_LEDS] = {LED1, LED2, LED3};

int potValue = 0; //value of the potentiometer
long sequenceDelay = 0;
int level = 0; // actual size of the sequence
int score = 0; // player score
int sequence[MAX_LIST_SIZE];
int clickCount = 0; // keeps track of the player clicks.
game_state currentState; // current state of the game.
long startTime = 0; // time the player starts to digit the sequence

//This variables are used to avoid bouncing buttons.
unsigned long lastDebounceTime1 = 0;  // last time the button1 was toggled.
unsigned long lastDebounceTime2 = 0;  // last time the button2 was toggled.
unsigned long lastDebounceTime3 = 0;  // last time the button3 was toggled.


void setup() {
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED_FLASH, OUTPUT);
  pinMode(T1, INPUT);
  pinMode(T2, INPUT);
  pinMode(T3, INPUT);
  pinMode(POT_PIN, INPUT);
  Serial.begin(9600);
  Serial.println("Welcome to Follow the Light!");
  randomSeed(analogRead(0)); //make sure the sequence is always random (pin 0 must be left floating to ensure better randomness).
  currentState = INIT;
}

void loop() {
  switch (currentState) {
    case INIT: {
        fading(LED_FLASH);
        int newValue = analogRead(POT_PIN);
        if (newValue != potValue) {
          potValue = newValue;
        }
        int button1State = digitalRead(T1);
        if (button1State == HIGH) {
          setSequenceDelay(); // sets the game speed according to the current potentiometer value
          currentState = STARTED;
        }
        break;
      }

    case STARTED: {
        Serial.println("Ready!");
        digitalWrite(LED_FLASH, LOW);
        addRandLed();
        currentState = SHOWING_LIGHTS;
        break;
      }

    case SHOWING_LIGHTS: {
        delay(1000); //wait some time before showing the sequence
        showLeds(sequenceDelay);
        startTime = millis();
        currentState = WAIT_USER;
        break;
      }

    case WAIT_USER: {
        if (millis() - startTime < PLAYER_INPUT_TIME) {
          int reading1 = digitalRead(T1);
          int reading2 = digitalRead(T2);
          int reading3 = digitalRead(T3);
          if (reading1 == HIGH && debouncedButton(lastDebounceTime1)) {
            lastDebounceTime1 = millis();
            pressButton(T1);
          } else if (reading2 == HIGH && debouncedButton(lastDebounceTime2)) {
            lastDebounceTime2 = millis();
            pressButton(T2);
          } else if (reading3 == HIGH  && debouncedButton(lastDebounceTime3)) {
            lastDebounceTime3 = millis();
            pressButton(T3);
          }
          if (clickCount == level) {
            score += level;
            clickCount = 0;
            addRandLed();
            currentState = SHOWING_LIGHTS;
          }
        } else {
          currentState = GAME_OVER;
        }
        break;
      }

    case GAME_OVER: {
        Serial.print("GAME OVER!, score: ");
        Serial.println(score * (MAX_SPEED / sequenceDelay) * 1000);
        blinkLed(LED_FLASH, DT_GAME_OVER);
        level = 0;
        score = 0;
        clickCount = 0;
        currentState = INIT;
        break;
      }
  }
}

/**
   Get the game speed according to the potentiometer value.
*/
void setSequenceDelay() {
  sequenceDelay = map(potValue, 0, POT_RANGE, MIN_SPEED, MAX_SPEED);
}

/**

*/
void pressButton(int button) {
  blinkLed(buttonToLed(button), 200);
  if (!check(button)) {
    currentState = GAME_OVER;;
  } else {
    clickCount++;
  }
}

/*
  blink every led in the sequence.
*/
void showLeds(long sequenceDelay) {
  for (int i = 0; i < level; i++) {
    blinkLed(sequence[i], sequenceDelay);
  }
}

/**
    Add a random led to the sequence.
*/
int addRandLed() {
  sequence[level] = leds[random(0, NUM_LEDS)];
  level++;
}

/**
   Return the led associated to the given button.
*/
int buttonToLed(int button) {
  switch (button) {
    case T1: return leds[0]; break;
    case T2: return leds[1]; break;
    case T3: return leds[2]; break;
  }
}

/*
   Checks if the player is pressing the right button.
*/
boolean check(int pressedButton) {
  return sequence[clickCount] == (buttonToLed(pressedButton));
}
