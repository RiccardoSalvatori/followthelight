#ifndef __UTILS__
#define __UTILS__

/**
   Turn on a led for a given amount of time.
*/
void blinkLed(int led, int delayTime);


/**
   Handle bouncing problems for buttons.
*/
boolean debouncedButton(unsigned long buttonLastDebounceTime);

/**
   Change current intensity of a given led.
*/
void fading(int led);

#endif
